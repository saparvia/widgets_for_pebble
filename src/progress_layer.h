/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file progress_layer.h
\brief A layer which shows a progress bar and a textual representation of the progress.

\image html example_progress_layer_basalt_0.png

**/

#pragma once
#include <pebble.h>
#include "stack_layer.h"
#include "progressbar_layer.h"
#include "util.h"

/** \struct ProgressLayer
\brief Data structure describing a ProgressLayer

Avoid accessing the members directly when possible, and instead use the progress_layer_* functions in progress_layer.h.
**/
typedef struct {
  StackLayer* stack_layer; ///< StackLayer which contains the progress bar and the associated text
  ProgressBarLayer* progressbar_layer; ///< The ProgressBar showing the progress
  TextLayer* text_layer; ///< The TextLayer which shows a textual representation of the progress
  char* text_layer_text; ///< Text that is shown by the text layer
} ProgressLayer;

/**
\brief Function to create a new ProgressLayer
**/
ProgressLayer* progress_layer_create(GRect frame, ///< Position and size of the new ProgressLayer
                                     float min_value, ///< New minimum value for progress
                                     float max_value ///< New maximum value for progress
                                    );

/**
\brief Free memory associated with a ProgressLayer
**/
void progress_layer_destroy(ProgressLayer* progress_layer ///< ProgressWindow to free
                           );

/**
\brief Return the Layer associated with the ProgressLayer

Returns a standard Pebble Layer which can be used with the standard Pebble functions
**/
Layer* progress_layer_get_layer(ProgressLayer* progress_layer ///< ProgressLayer for which to get the associated Layer
                               );

/**
\brief Return the StackLayer associated with the ProgressLayer

**/
StackLayer* progress_layer_get_stack_layer(ProgressLayer* progress_layer ///< ProgressLayer for which to get the associated StackLayer
                               );

/**
\brief Sets the range of the progress bar

Set the range of the progress bar. This determines how large a fraction of the progress bar should be filled for a given progress value
**/
void progress_layer_set_range(ProgressLayer* progress_layer, ///< ProgressLayer for which to set the new range
                              float min_value, ///< New minimum value for progress
                              float max_value ///< New maximum value for progress
                             );

/**
\brief Sets the progress value of the progress bar, and the text displayed beneath the progress bar

Set the value of the progress bar to \a value. The value is clipped to the minimum / maximum set by progress_window_set_range.
The text \a text is displayed beneath the progress bar. This could be e.g. a textual representation of the progress ("47%") or a description of what is going on right now ("Retrieving data...")
**/
void progress_layer_set_value(ProgressLayer* progress_layer, ///< ProgressLayer for which to set the value
                              float value, ///< Value for progress
                              const char* text ///< Text for progress
                             );

/**
\brief Get the current value of the progress
**/
float progress_layer_get_value(ProgressLayer* progress_layer ///< ProgressLayer for which to get the value
                              );

/**
\brief Get the text shown
**/
const char* progress_layer_get_text(ProgressLayer* progress_layer ///< ProgressLayer for which to get the text
                                   );

/**
\brief Get the maximum value of the progress value
**/
float progress_layer_get_max_value(ProgressLayer* progress_layer ///< ProgressLayer for which to get maximum progress value
                                  );

/**
\brief Get the minimum value of the progress value
**/
float progress_layer_get_min_value(ProgressLayer* progress_layer ///< ProgressLayer for which to get minimum progress value
                                  );

/* vim: set expandtab tabstop=2 shiftwidth=2: */
