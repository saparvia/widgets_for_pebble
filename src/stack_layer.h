/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once
#include <pebble.h>

/**
\file stack_layer.h
\brief A layer which stacks child Layers either beneath or next to each other.

\image html example_stack_layer_basalt_0.png

Layers added to a StackLayer will automatically be placed so that they are beneath or next to each other, depending on the chosen orientation of the layout.

Layers will be resized so that they maintain their original size in the stacking direction, while the other dimension is resized to match the size of the parent StackLayer. By calling stack_layer_set_fill_last the layer can be resized to fill any remaining space in the parent StackLayer.

Padding can optionally be added to the child layers, either for all children, by calling stack_layer_set_padding(), or on a per-child basis, using stack_layer_child_set_padding().
**/

/**
\brief Possible orientations for a StackLayer
**/
typedef enum { STACK_HORIZONTAL, ///< Stack Layers next to each other
              STACK_VERTICAL ///< Stack Layers beneath each other
             } stack_orientation_t;

typedef struct StackLayer StackLayer;

/**
\brief Data structure describing a child of a StackLayer

Avoid accessing these members directly, and instead use the provided stack_layer_child_* functions in stack_layer.h.
**/
typedef struct {
  Layer* layer; ///< Layer associated with a StackLayer child
  StackLayer* stack_layer; ///< If child is also a StackLayer, pointer to it is stored here
  int top_padding; ///< Padding at the top
  int bottom_padding; ///< Padding at the bottom
  int left_padding; ///< Padding to the left
  int right_padding; ///< Padding to the right
  GRect original_frame; ///< Original frame of the child, before any auto-resizing
} StackLayerChild;

/**
\brief Data structure describing a StackLayer

Avoid accessing these members directly, and instead use the provided stack_layer_* functions in stack_layer.h.
**/
struct StackLayer {
  stack_orientation_t orientation; ///< Orientation of the StackWidget
  Layer* layer; ///< The associated Layer
  uint32_t nchildren; ///< Number of child layers
  StackLayerChild* children; ///< Array of child Layers
  int xpadding; ///< Padding to use in the x-direction
  int ypadding; ///< Padding to use in the y-direction
  bool fill_last; ///< Resize the last child to fill the remaining space
};

/**
\brief Create a new StackLayer

Create a new StackLayer with the given orientation
**/
StackLayer* stack_layer_create(GRect frame, ///< Position and size of the new StackLayer
                               stack_orientation_t orientation ///< Orientation of the new StackLayer
                              );

/**
\brief Free the memory associated with a StackLayer

Free the memory associated with a StackLayer. Note that Layers added to the StackLayer are NOT freed.
**/
void stack_layer_destroy(StackLayer* stack_layer ///< StackLayer to free
                        );
/**
\brief Return the Layer associated with the StackLayer
**/
Layer* stack_layer_get_layer(StackLayer* stack_layer ///< StackLayer for which to get the associated Layer
                            );

/**
\brief Insert a Layer into the StackLayer

Insert a Layer \a layer_to_insert into the StackLayer \a stack_layer at position \a position. If @a position < 0, the position is counted from the end of the stack.
\return The actual position where the Layer was inserted
**/
int stack_layer_insert_layer(StackLayer* stack_layer, ///< StackLayer into which to insert
                              int position, ///< Position in StackLayer where to insert the new Layer
                              Layer* layer_to_insert ///< The Layer to insert
                             );

/**
\brief Insert a StackLayer into the StackLayer

Insert a StackLayer \a layer_to_insert into the StackLayer \a stack_layer at position \a position. If @a position < 0, the position is counted from the end of the stack. Use this function instead of stack_layer_add_layer to ensure that the child StackLayer layout is updated when the layout of the parent changes
\return The actual position where the StackLayer was inserted
**/
int stack_layer_insert_stack_layer(StackLayer* stack_layer, ///< StackLayer into which to insert
                              int position, ///< Position in StackLayer where to insert the new Layer
                              StackLayer* layer_to_insert ///< The StackLayer to insert
                             );

/**
\brief Add a new child Layer to a StackLayer

Add a Layer \a layer_to_add to the StackLayer \a stack_layer
**/
void stack_layer_add_layer(StackLayer* stack_layer, ///< StackLayer to which to add the new Layer
                           Layer* layer_to_add ///< Layer to add to the StackLayer
                          );

/**
\brief Add a new child StackLayer to a StackLayer

Add a StackLayer \a layer_to_add to the StackLayer \a stack_layer.  Use this function instead of stack_layer_add_layer to ensure that the child StackLayer layout is updated when the layout of the parent changes
**/
void stack_layer_add_stack_layer(StackLayer* stack_layer, ///< StackLayer to which to add the new Layer
                           StackLayer* layer_to_add ///< StackLayer to add to the StackLayer
                          );

/**
\brief Recalculate the size of child layers
  
Recalculate the sizes of child layers so they match the size of \a stack_layer. You need to call this function whenever the size of \a stack_layer changes
**/
void stack_layer_recalculate_layout(StackLayer* stack_layer ///< Stack layer whose layout should be recalculated
                                   );

/**
\brief Set padding around child layers

Set the padding around child layers. In the stacking direction layers are separated by the padding, without affecting their size in that direction, while the size in the other direction is decreased to make room for the padding. 
**/
void stack_layer_set_padding(StackLayer* stack_layer, ///< StackLayer whose padding should be changed
                             int xpadding, ///< New padding in the x-direction
                             int ypadding ///< New padding in the y-direction
                            );

/**
\brief Get the padding in the x-direction
**/
int stack_layer_get_xpadding(StackLayer* stack_layer ///< StackLayer whose padding in the x-direction should be retrieved
                            );

/**
\brief Get the padding in the y-direction
**/
int stack_layer_get_ypadding(StackLayer* stack_layer ///< StackLayer whose padding in the y-direction should be retrieved
                            );

/**
\brief Return the children of the StackLayer
**/
StackLayerChild* stack_layer_get_children(StackLayer* stack_layer, ///< StackLayer whose children should be returned
                                           int* nchildren ///< Number of children will be stored here if not NULL
                                         );

/**
\brief Set whether the last child should be resized to fill the remaining space in the StackLayer

**/
void stack_layer_set_fill_last(StackLayer* stack_layer, ///< StackLayer
                               bool fill_last ///< true if last child should be resized to fill remaining space, false otherwise
                               );

/**
\brief Return whether the last child is resized to fill the remaining space in the StackLayer

**/
bool stack_layer_get_fill_last(StackLayer* stack_layer);
                               
/**
\brief Set the padding of an individual child

**/
void stack_layer_child_set_padding(StackLayerChild* stack_layer_child, ///< Child whose padding should be set
                                   int left_padding, ///< Padding to the left of the child
                                   int right_padding, ///< Padding to the right of the child
                                   int top_padding, ///< Padding to the top of the child
                                   int bottom_padding ///< Padding to the bottom of the child
                                  );

/**
\brief Get the padding of an individual child

**/
void stack_layer_child_get_padding(StackLayerChild* stack_layer_child, ///< Child whose padding should be got
                                   int* left_padding, ///< Padding to the left of the child
                                   int* right_padding, ///< Padding to the right of the child
                                   int* top_padding, ///< Padding to the top of the child
                                   int* bottom_padding ///< Padding to the bottom of the child
                                  );

/* vim: set expandtab tabstop=2 shiftwidth=2: */
