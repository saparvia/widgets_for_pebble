/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file progress_window.h
\brief A window which shows a title and a ProgressLayer.

\image html example_progress_window_basalt_0.png

**/

#pragma once
#include <pebble.h>
#include "progress_layer.h"
#include "stack_layer.h"

/** \struct ProgressWindow
\brief Data structure describing a ProgressWindow

Avoid accessing the members directly when possible, and instead use the progress_window_* functions in progress_window.h.
**/
typedef struct {
  Window* window; ///< The actual Window
  ProgressLayer* progress_layer; ///< ProgressLayer showing progress bar
  TextLayer* title_layer; ///< TextLayer showing title
  StackLayer* stack_layer; ///< StackLayer containing the title and progress layers
} ProgressWindow;

/**
\brief Function to create a new ProgressWindow
**/
ProgressWindow* progress_window_create(const char* title, ///< Title of the window
                                       int min_value, ///< Minimum value for the progress bar
                                       int max_value ///< Maximum value for the progress bar
                                      );

/**
\brief Free memory associated with a ProgressWindow
**/
void progress_window_destroy(ProgressWindow* progress_window ///< ProgressWindow to free
                            );

/**
\brief Return the Window associated with the ProgressWindow

Returns a standard Pebble Window which can be used with the standard Pebble functions
**/
Window* progress_window_get_window(ProgressWindow* progress_window ///< ProgressWindow for which to retrieve the Window
                                  );

/**
\brief Sets the range of the progress bar

Set the range of the progress bar. This determines how large a fraction of the progress bar should be filled for a given progress value
**/
void progress_window_set_range(ProgressWindow* progress_window, ///< ProgressWindow for which to set the range
                               float min_value, ///< New minimum value for progress
                               float max_value ///< New maximum value for progress
                              );

/**
\brief Sets the progress value of the progress bar, and the text displayed beneath the progress bar

Set the value of the progress bar to \a value. The value is clipped to the minimum / maximum set by progress_window_set_range.
The text \a text is displayed beneath the progress bar. This could be e.g. a textual representation of the progress ("47%") or a description of what is going on right now ("Retrieving data...")
**/
void progress_window_set_value(ProgressWindow* progress_window, ///< ProgressWindow for which to set the value
                               float value, ///< Value for progress
                               const char* text ///< Text for progress
                              );

/**
\brief Get the current value of the progress
**/
float progress_window_get_value(ProgressWindow* progress_window ///< ProgressWindow for which to get the value
                               );

/**
\brief Get the text shown
**/
const char* progress_window_get_text(ProgressWindow* progress_window ///< ProgressWindow for which to get the text
                                    );

/**
\brief Get the maximum value of the progress value
**/
float progress_window_get_max_value(ProgressWindow* progress_window ///< ProgressWindow for which to get maximum progress value
                                   );

/**
\brief Get the minimum value of the progress value
**/
float progress_window_get_min_value(ProgressWindow* progress_window ///< ProgressWindow for which to get maximum progress value
                                   );

/* vim: set expandtab tabstop=2 shiftwidth=2: */
