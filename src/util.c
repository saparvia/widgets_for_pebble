/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <pebble.h>
#include "util.h"

void* insert_into_array(void* array, void* item, int32_t position, size_t item_size, int32_t num_elements) {
  if(position < 0) {
    position = num_elements + position + 1;
  }
  
  void* new_array = realloc(array, (num_elements+1)*item_size);
  
  if(position != num_elements) {
    memmove(new_array + (position+1)*item_size, new_array + position*item_size, (num_elements - position)*item_size);
  }

  memcpy(new_array + position*item_size, item, item_size);
  return new_array;
}

char* strdup (const char* str) {
    char* retval = malloc(strlen(str) + 1);
    if (retval == NULL) return retval;
    strncpy(retval, str, strlen(str)+1);
    return retval;
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
