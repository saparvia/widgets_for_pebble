/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/
#pragma once

/**
\file util.h
\brief Common utility functions that are not directly related to Pebble development

**/

/**
\brief Insert an element into an array

Insert element \a item of size \a item_size into array \a array at position \a position. The old element at \a position, and elements following it, are shifted one step forward.
If \a position is negative, the position is counted from the end of the array.

\return The new array with the item inserted.
**/

void* insert_into_array(void* array, ///< Array into which to insert
                        void* item, ///< Element to insert
                        int32_t position, ///< Position at which to insert
                        size_t item_size, ///< Size of item
                        int32_t num_elems ///< Number of elemenets in array
                       );

/**
\brief Duplicate a string

Returns a new copy of the given string, by first allocating enough memory, and then copying the contents of the string
**/
char* strdup (const char *s ///< String to duplicate
             );
/* vim: set expandtab tabstop=2 shiftwidth=2: */
