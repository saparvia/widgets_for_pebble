/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Layer that shows a progress bar and text to indicate progress
*/

#include <pebble.h>
#include "progress_layer.h"

ProgressLayer* progress_layer_create(GRect frame, float min_value, float max_value) {

  ProgressLayer* new_layer = malloc(sizeof(ProgressLayer));
  new_layer->stack_layer = stack_layer_create(frame, STACK_VERTICAL);
  stack_layer_set_fill_last(new_layer->stack_layer, true);
  GRect rect;
  rect.size.w = 0;
  rect.origin.x = 0;
  rect.origin.y = 0;

  rect.size.h = 15;
  new_layer->progressbar_layer = progressbar_layer_create(rect, min_value, max_value);

  rect.size.h = 20;
  new_layer->text_layer = text_layer_create(rect);
  new_layer->text_layer_text = 0;
  text_layer_set_text_alignment(new_layer->text_layer, GTextAlignmentCenter);
  

  stack_layer_add_layer(new_layer->stack_layer, progressbar_layer_get_layer(new_layer->progressbar_layer));
  
  stack_layer_add_layer(new_layer->stack_layer, text_layer_get_layer(new_layer->text_layer));

  return new_layer;
}

void progress_layer_destroy(ProgressLayer* progress_layer) {
  stack_layer_destroy(progress_layer->stack_layer);
  progressbar_layer_destroy(progress_layer->progressbar_layer);
  text_layer_destroy(progress_layer->text_layer);
  
  free(progress_layer);
}

Layer* progress_layer_get_layer(ProgressLayer* progress_layer) {
  return stack_layer_get_layer(progress_layer->stack_layer);
}

StackLayer* progress_layer_get_stack_layer(ProgressLayer* progress_layer) {
  return progress_layer->stack_layer;
}

void progress_layer_set_range(ProgressLayer* progress_layer, float min_value, float max_value) {
  progressbar_layer_set_range(progress_layer->progressbar_layer, min_value, max_value);
}

void progress_layer_set_value(ProgressLayer* progress_layer, float value, const char* text) {
  progressbar_layer_set_value(progress_layer->progressbar_layer, value);
  if(progress_layer->text_layer_text) free(progress_layer->text_layer_text);
  
  // In principle we could also use a pre-allocated buffer. But this is more general, and it's unlikely to be a performance problem
  progress_layer->text_layer_text = strdup(text);
  
  text_layer_set_text(progress_layer->text_layer, progress_layer->text_layer_text);
}

float progress_layer_get_value(ProgressLayer* progress_layer) {
  return progressbar_layer_get_value(progress_layer->progressbar_layer);
}

const char* progress_layer_get_text(ProgressLayer* progress_layer) {
  return text_layer_get_text(progress_layer->text_layer);
}

float progress_layer_get_max_value(ProgressLayer* progress_layer) {
  return progressbar_layer_get_min_value(progress_layer->progressbar_layer);
}

float progress_layer_get_min_value(ProgressLayer* progress_layer) {
    return progressbar_layer_get_max_value(progress_layer->progressbar_layer);
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
