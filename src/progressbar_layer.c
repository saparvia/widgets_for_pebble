/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Layer that shows a progress bar
*/

#include <pebble.h>
#include "progressbar_layer.h"

static void draw(Layer* layer, GContext* ctx) {
  ProgressBarLayer* progressbar_layer = layer_get_data(layer);
  float frac = (progressbar_layer->current_value - progressbar_layer->min_value)/(progressbar_layer->max_value - progressbar_layer->min_value);
  GRect rect = layer_get_bounds(layer);
  
  graphics_draw_round_rect(ctx, rect, 3);

  rect.size.w = rect.size.w * frac;
  
  graphics_context_set_fill_color(ctx, GColorBlack);
  graphics_fill_rect(ctx, rect, 3, GCornersAll);
}

ProgressBarLayer* progressbar_layer_create(GRect frame, float min_value, float max_value) {
  Layer* layer = layer_create_with_data(frame, sizeof(ProgressBarLayer));

  ProgressBarLayer* new_layer = layer_get_data(layer);
  new_layer->layer = layer;
  progressbar_layer_set_range(new_layer, min_value, max_value);
  progressbar_layer_set_value(new_layer, min_value);
  
  layer_set_update_proc(new_layer->layer, draw);
  return new_layer;
}

Layer* progressbar_layer_get_layer(ProgressBarLayer* progressbar_layer) {
  return progressbar_layer->layer;
}

void progressbar_layer_set_range(ProgressBarLayer* progressbar_layer, float min_value, float max_value) {
  progressbar_layer->min_value = min_value;
  progressbar_layer->max_value = max_value;
  if(progressbar_layer->current_value > max_value) {
    progressbar_layer->current_value = max_value;
  }
  else if(progressbar_layer->current_value < min_value) {
    progressbar_layer->current_value = min_value;
  }
  
  layer_mark_dirty(progressbar_layer_get_layer(progressbar_layer));
}

void progressbar_layer_set_value(ProgressBarLayer* progressbar_layer, float value) {
  progressbar_layer->current_value = value;
  float max_value = progressbar_layer_get_max_value(progressbar_layer);
  float min_value = progressbar_layer_get_min_value(progressbar_layer);
  
  if(progressbar_layer->current_value > max_value) {
    progressbar_layer->current_value = max_value;
  }
  else if(progressbar_layer->current_value < min_value) {
    progressbar_layer->current_value = min_value;
  }
  
  layer_mark_dirty(progressbar_layer_get_layer(progressbar_layer));
}

float progressbar_layer_get_value(ProgressBarLayer* progressbar_layer) {
  return progressbar_layer->current_value;
}

float progressbar_layer_get_max_value(ProgressBarLayer* progressbar_layer) {
  return progressbar_layer->max_value;
}

float progressbar_layer_get_min_value(ProgressBarLayer* progressbar_layer) {
  return progressbar_layer->min_value;
}

void progressbar_layer_destroy(ProgressBarLayer* progress_layer) {
  layer_destroy(progress_layer->layer);
  //free(progress_layer);
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
