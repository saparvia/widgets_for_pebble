/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Layer that stacks added layers on top of / next to each other
*/

#include <pebble.h>
#include "stack_layer.h"
#include "util.h"

StackLayer* stack_layer_create(GRect frame, stack_orientation_t orientation) {
  Layer* new_layer = layer_create(frame);
  
  StackLayer* new_stack = malloc(sizeof(StackLayer));
  new_stack->orientation = orientation;
  new_stack->layer = new_layer;
  new_stack->nchildren = 0;
  new_stack->children = 0;
  stack_layer_set_padding(new_stack, 0, 0);
  
  return new_stack;
}

void stack_layer_destroy(StackLayer* stack_layer) {
  layer_destroy(stack_layer->layer);
  free(stack_layer->children);
  free(stack_layer);
}

Layer* stack_layer_get_layer(StackLayer* stack_layer) {
  return stack_layer->layer;
}


int stack_layer_insert_layer(StackLayer* stack_layer, int position, Layer* layer_to_insert) {
  Layer* layer = stack_layer_get_layer(stack_layer);
  
  StackLayerChild* new_child = malloc(sizeof(StackLayerChild));
  new_child->original_frame = layer_get_frame(layer_to_insert);
  new_child->layer = layer_to_insert;
  new_child->stack_layer = 0;
  
  new_child->top_padding = stack_layer_get_ypadding(stack_layer);
  new_child->bottom_padding = stack_layer_get_ypadding(stack_layer);
  new_child->left_padding = stack_layer_get_xpadding(stack_layer);
  new_child->right_padding = stack_layer_get_xpadding(stack_layer);
  
  stack_layer->children = insert_into_array(stack_layer->children, new_child, position, sizeof(StackLayerChild), stack_layer->nchildren);
  free(new_child);
  
  layer_add_child(layer, layer_to_insert);  
  stack_layer->nchildren++;
  
  ///@todo No need to recalculate the full thing ...
  stack_layer_recalculate_layout(stack_layer);
  
  if(position < 0) {
    return stack_layer->nchildren + position;
  }
  else {
    return position;
  }
  
}

int stack_layer_insert_stack_layer(StackLayer* stack_layer, int position, StackLayer* stack_layer_to_insert) {
  int pos = stack_layer_insert_layer(stack_layer, position, stack_layer_get_layer(stack_layer_to_insert));
  stack_layer->children[pos].stack_layer = stack_layer_to_insert;
  
  stack_layer_recalculate_layout(stack_layer); // This is already called once in the normal insert above, so a bit wasteful
  
  return pos;
}

void stack_layer_recalculate_layout(StackLayer* stack_layer) {
  Layer* layer = stack_layer_get_layer(stack_layer);
  bool fill_last = stack_layer_get_fill_last(stack_layer);
  
  GRect stack_frame = layer_get_frame(layer);

  int child_count;
  StackLayerChild* children = stack_layer_get_children(stack_layer, &child_count);
  for(int i = 0; i < child_count; i++) {
    GRect new_frame = layer_get_frame(children[i].layer);
    
    GRect original_frame = children[i].original_frame;
    new_frame.size = original_frame.size; // Always start of with the original size
    
    int left_padding, right_padding, top_padding, bottom_padding;
    stack_layer_child_get_padding(&children[i], &left_padding, &right_padding, &top_padding, &bottom_padding);
    
    if(stack_layer->orientation == STACK_VERTICAL) {
      new_frame.size.w = stack_frame.size.w - (left_padding + right_padding);
    }
    else {
      new_frame.size.h = stack_frame.size.h - (top_padding + bottom_padding);
    }
    
    new_frame.origin.x = left_padding;
    new_frame.origin.y = top_padding;

    if(i >= 1) {
      GRect prev_frame = layer_get_frame(children[i-1].layer);
      
      int prev_left_padding, prev_right_padding, prev_top_padding, prev_bottom_padding;
      stack_layer_child_get_padding(&children[i-1], &prev_left_padding, &prev_right_padding, &prev_top_padding, &prev_bottom_padding);
      
      if(stack_layer->orientation == STACK_VERTICAL) {
        new_frame.origin.y = prev_frame.origin.y + prev_frame.size.h + prev_bottom_padding + top_padding;
      }
      else{
        new_frame.origin.x = prev_frame.origin.x + prev_frame.size.w + prev_right_padding + left_padding;
      }
    }
    
    // Make the last child fill the remaining space
    if(fill_last && (i == child_count - 1)) {
      if(stack_layer->orientation == STACK_VERTICAL) {
        new_frame.size.h = stack_frame.size.h - new_frame.origin.y - bottom_padding;
      }
      else {
        new_frame.size.w = stack_frame.size.w - new_frame.origin.x - right_padding;
      }
    }
    layer_set_frame(children[i].layer, new_frame);
    
    #ifdef PBL_SDK_2 // Work around issue PBL-30455 in Aplite with SDK 2
    GRect new_bounds = new_frame;
    new_bounds.origin.x = 0;
    new_bounds.origin.y = 0;
    layer_set_bounds(children[i].layer, new_bounds);
    #endif
    
    if(stack_layer->children[i].stack_layer) {
      // If a StackLayer includes itself as a child this will loop forever
      stack_layer_recalculate_layout(children[i].stack_layer);
    }  
  }
}

void stack_layer_add_layer(StackLayer* stack_layer, Layer* layer_to_add) {
  stack_layer_insert_layer(stack_layer, -1, layer_to_add);
}

void stack_layer_add_stack_layer(StackLayer* stack_layer, StackLayer* stack_layer_to_add) {
  stack_layer_insert_stack_layer(stack_layer, -1, stack_layer_to_add);
}

StackLayerChild* stack_layer_get_children(StackLayer* stack_layer, int* nchildren) {
  if(nchildren) {
    *nchildren = stack_layer->nchildren; 
  }
  return stack_layer->children;
}

void stack_layer_set_padding(StackLayer* stack_layer, int xpadding, int ypadding) {
  stack_layer->xpadding = xpadding;
  stack_layer->ypadding = ypadding;
  int nchildren;
  StackLayerChild* children = stack_layer_get_children(stack_layer, &nchildren);
  for(int i = 0; i< nchildren; i++) {
    stack_layer_child_set_padding(&children[i], xpadding, xpadding, ypadding, ypadding);
  }
  stack_layer_recalculate_layout(stack_layer);
}

int stack_layer_get_xpadding(StackLayer* stack_layer) {
  return stack_layer->xpadding;
}

int stack_layer_get_ypadding(StackLayer* stack_layer) {
  return stack_layer->ypadding;
}

void stack_layer_set_fill_last(StackLayer* stack_layer, bool fill_last) {
  stack_layer->fill_last = fill_last;
}

bool stack_layer_get_fill_last(StackLayer* stack_layer) {
  return stack_layer->fill_last;
}
void stack_layer_child_set_padding(StackLayerChild* stack_layer_child, int left_padding, int right_padding, int top_padding, int bottom_padding) {
  stack_layer_child->left_padding = left_padding;
  stack_layer_child->right_padding = right_padding;
  stack_layer_child->top_padding = top_padding;
  stack_layer_child->bottom_padding = bottom_padding;
}

void stack_layer_child_get_padding(StackLayerChild* stack_layer_child, int *left_padding, int *right_padding, int *top_padding, int *bottom_padding) {
  *left_padding = stack_layer_child->left_padding;
  *right_padding = stack_layer_child->right_padding;
  *top_padding = stack_layer_child->top_padding;
  *bottom_padding = stack_layer_child->bottom_padding;
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
