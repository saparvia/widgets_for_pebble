/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\file progressbar_layer.h
\brief A layer which shows a progress bar

\image html example_progressbar_layer_basalt_0.png

**/

#pragma once
#include <pebble.h>

/** \struct ProgressBarLayer
\brief Data structure describing a ProgressBarLayer

Avoid accessing the members directly when possible, and instead use the progressbar_layer_* functions in progressbar_layer.h.
**/
typedef struct {
  Layer* layer; ///< Layer associated with the ProgressLayer
  float min_value; ///< Minimum value for the progress
  float max_value; ///< Maximum value for the progress
  float current_value; ///< Current value for the progress
} ProgressBarLayer;

/**
\brief Function to create a new ProgressBarLayer
**/
ProgressBarLayer* progressbar_layer_create(GRect frame, ///< Position and size of the new ProgressBarLayer
                                           float min_value, ///< New minimum value for progress
                                           float max_value ///< New maximum value for progress
                                          );

/**
\brief Free memory associated with a ProgressBarLayer
**/
void progressbar_layer_destroy(ProgressBarLayer* progress_layer
                              );

/**
\brief Return the Layer associated with the ProgressBarLayer

Returns a standard Pebble Layer which can be used with the standard Pebble functions
**/
Layer* progressbar_layer_get_layer(ProgressBarLayer* progress_layer ///< ProgressBarLayer for which to retrieve the associated layer
                                  );

/**
\brief Sets the range of the progress bar

Set the range of the progress bar. This determines how large a fraction of the progress bar should be filled for a given progress value
**/
void progressbar_layer_set_range(ProgressBarLayer* progress_layer,
                                 float min_value,float max_value
                                );

/**
\brief Sets the progress value of the progress bar, and the text displayed beneath the progress bar

Set the value of the progress bar to \a value. The value is clipped to the minimum / maximum set by progress_window_set_range.
The text \a text is displayed beneath the progress bar. This could be e.g. a textual representation of the progress ("47%") or a description of what is going on right now ("Retrieving data...")
**/
void progressbar_layer_set_value(ProgressBarLayer* progress_layer, ///< ProgressBarLayer whose progrss value is to be set
                                 float value ///< The new value for the progress bar
                                );

/**
\brief Get the current value of the progress
**/
float progressbar_layer_get_value(ProgressBarLayer* progress_layer
                                 );

/**
\brief Get the maximum value of the progress value
**/
float progressbar_layer_get_max_value(ProgressBarLayer* progress_layer
                                     );

/**
\brief Get the minimum value of the progress value
**/
float progressbar_layer_get_min_value(ProgressBarLayer* progress_layer
                                     );

/* vim: set expandtab tabstop=2 shiftwidth=2: */
