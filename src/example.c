/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
\example example_demo.c

Demo app for demonstrating Widgets for Pebble features

\image html example_demo_basalt_0.png

\image html example_demo_basalt_1.png

**/

// TOUR: S

#include <pebble.h>
#include "stack_layer.h"
#include "progressbar_layer.h"
#include "progress_layer.h"
#include "progress_window.h"

Window *my_window = 0;
TextLayer* labels[3] = {0,0,0};
StackLayer* main_stack = 0;
StackLayer* left_stack = 0;
StackLayer* right_stack = 0;
ProgressBarLayer* progressbar_layer;
ProgressLayer* progress_layer;
ProgressWindow* progress_window;

static void timer(void* data) {
  ProgressWindow* progress_window = data;
  double value = progress_window_get_value(progress_window);
  char buf[256];
  value += 0.1;
  snprintf(buf,256, "Progress: %d%%", (int)(value*100));

  progress_window_set_value(progress_window, value, buf);
  if(value < 1.0) {
    app_timer_register(500, timer, progress_window);  
  }
}

static void mid_click_handler(ClickRecognizerRef recognizer, void *context) {
  APP_LOG(APP_LOG_LEVEL_DEBUG, "Click!");
  window_stack_push(progress_window_get_window(progress_window), true);
  app_timer_register(500, timer, progress_window);
}

static void click_config_provider(void *context) {
  window_single_click_subscribe(BUTTON_ID_SELECT, mid_click_handler);
}


void handle_init(void) { 
  my_window = window_create();
  window_stack_push(my_window, true);

  Layer* main_layer = window_get_root_layer(my_window);
  GRect main_frame = layer_get_frame(main_layer);  
  GRect frame = main_frame;
  
  main_stack = stack_layer_create(frame, STACK_HORIZONTAL);
  layer_add_child(main_layer, stack_layer_get_layer(main_stack));
  
  frame.size.w = main_frame.size.w * 0.60;
  left_stack = stack_layer_create(frame, STACK_VERTICAL);
  frame.size.w = main_frame.size.w * 0.40;
  right_stack = stack_layer_create(frame, STACK_VERTICAL);
  
  stack_layer_add_layer(main_stack, stack_layer_get_layer(left_stack));
  stack_layer_add_layer(main_stack, stack_layer_get_layer(right_stack));
  
  stack_layer_set_padding(left_stack, 5, 5);
  stack_layer_set_padding(right_stack, 5, 5);
  
  labels[0] = text_layer_create(GRect(0, 0, 20, 20));
  text_layer_set_text(labels[0], "ProgressBar");
  text_layer_set_font(labels[0], fonts_get_system_font(FONT_KEY_GOTHIC_14));
  labels[1] = text_layer_create(GRect(0, 0, 20, 20));
  text_layer_set_text(labels[1], "Progress");
  text_layer_set_font(labels[1], fonts_get_system_font(FONT_KEY_GOTHIC_14));
  labels[2] = text_layer_create(GRect(0, 0, 40, 70));
  text_layer_set_text(labels[2], "Press middle to show ProgressWindow");
  text_layer_set_font(labels[2], fonts_get_system_font(FONT_KEY_GOTHIC_14));
  
  stack_layer_add_layer(left_stack, text_layer_get_layer(labels[0]));
  stack_layer_add_layer(left_stack, text_layer_get_layer(labels[1]));
  stack_layer_add_layer(left_stack, text_layer_get_layer(labels[2]));

  // ProgressBar
  frame.size.h = 20;

  progressbar_layer = progressbar_layer_create(frame, 0, 1);
  progressbar_layer_set_value(progressbar_layer, 0.33);
  
  stack_layer_add_layer(right_stack, progressbar_layer_get_layer(progressbar_layer));

  // Progress
  frame.size.h = 45;
  progress_layer = progress_layer_create(frame, 0, 1);
  progress_layer_set_value(progress_layer, 0.75, "75%");
 
  stack_layer_add_stack_layer(right_stack, progress_layer_get_stack_layer(progress_layer)); 

  // Progress window
  progress_window = progress_window_create("ProgressWindow", 0, 1);
  progress_window_set_value(progress_window, 0.0, "Doing stuff...");
  window_set_click_config_provider(my_window, click_config_provider);
}

void handle_deinit(void) {
  text_layer_destroy(labels[0]);
  text_layer_destroy(labels[1]);
  text_layer_destroy(labels[2]);
  progressbar_layer_destroy(progressbar_layer);
  progress_layer_destroy(progress_layer);
  stack_layer_destroy(right_stack);
  stack_layer_destroy(left_stack);
  stack_layer_destroy(main_stack);
  window_destroy(my_window);
}

int main(void) {
  handle_init();
  app_event_loop();
  handle_deinit();
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
