/*
Copyright Stefan Parviainen 2015

This file is part of Widgets for Pebble.

Widgets for Pebbe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Widgets for Pebble is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Widgets for Pebble.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <pebble.h>
#include "progress_window.h"

static void progress_window_onload(Window* window) {
  ProgressWindow* progress_window = window_get_user_data(window);
  GRect frame = layer_get_frame(window_get_root_layer(window));
  frame.origin.y = PBL_IF_ROUND_ELSE((frame.size.h-60)/2-20, 5);
  layer_set_frame(stack_layer_get_layer(progress_window->stack_layer), frame);
  stack_layer_recalculate_layout(progress_window->stack_layer);
}

ProgressWindow* progress_window_create(const char* title, int min_value, int max_value) {
  ProgressWindow* new_window = malloc(sizeof(ProgressWindow));
  new_window->window = window_create();
  window_set_user_data(new_window->window, new_window);
  
  window_set_window_handlers(new_window->window, (WindowHandlers){
    .load = progress_window_onload
  });
  
  GRect frame;
  frame.origin.x = 0;
  frame.origin.y = 0;
  frame.size.h = 50;
  frame.size.w = 0;
  
  new_window->stack_layer = stack_layer_create(frame, STACK_VERTICAL);
  stack_layer_set_padding(new_window->stack_layer, 10, 10);
  
  new_window->progress_layer = progress_layer_create(frame, min_value, max_value);
  
  TextLayer* title_layer = text_layer_create(frame);
  new_window->title_layer = title_layer;
  text_layer_set_text(title_layer, title);
  text_layer_set_text_alignment(title_layer, GTextAlignmentCenter);
  text_layer_set_font(title_layer, fonts_get_system_font(FONT_KEY_GOTHIC_24_BOLD));
  
  stack_layer_add_layer(new_window->stack_layer, text_layer_get_layer(title_layer));
  stack_layer_add_stack_layer(new_window->stack_layer, progress_layer_get_stack_layer(new_window->progress_layer));

  stack_layer_child_set_padding(&stack_layer_get_children(new_window->stack_layer, NULL)[1], 30, 30, 0, 0);
  
  Layer* root_layer = window_get_root_layer(new_window->window);
  layer_add_child(root_layer, stack_layer_get_layer(new_window->stack_layer));
  
  return new_window;
}

void progress_window_destroy(ProgressWindow* progress_window) {
  progress_layer_destroy(progress_window->progress_layer);
  text_layer_destroy(progress_window->title_layer);
  
  window_destroy(progress_window->window);

  free(progress_window);
}

Window* progress_window_get_window(ProgressWindow* progress_window) {
  return progress_window->window;
}

void progress_window_set_range(ProgressWindow* progress_window, float min_value, float max_value) {
  progress_layer_set_range(progress_window->progress_layer, min_value, max_value);
}

void progress_window_set_value(ProgressWindow* progress_window, float value, const char* text) {
  if(value >= progress_window_get_max_value(progress_window)) {
    window_stack_remove(progress_window_get_window(progress_window), true);
  }
  progress_layer_set_value(progress_window->progress_layer, value, text);
}

float progress_window_get_value(ProgressWindow* progress_window) {
  return progress_layer_get_value(progress_window->progress_layer);
}

const char* progress_window_get_text(ProgressWindow* progress_window) {
  return progress_layer_get_text(progress_window->progress_layer);
}

float progress_window_get_max_value(ProgressWindow* progress_window) {
  return progress_layer_get_min_value(progress_window->progress_layer);
}

float progress_window_get_min_value(ProgressWindow* progress_window) {
    return progress_layer_get_max_value(progress_window->progress_layer);
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
