/**
\example example_progress_window.c

An example on how to use progress_window.h

\image html example_progress_window_basalt_0.png

**/

#include <pebble.h>
#include <progress_window.h>

int main(void) {
  ProgressWindow* progress_window = progress_window_create("ProgressWindow", 0, 1);
  progress_window_set_value(progress_window, 0.33, "Doing stuff...");
  window_stack_push(progress_window_get_window(progress_window), true);
  app_event_loop();
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
