/**
\example example_progress_layer.c

An example of how to use progress_layer.h

\image html example_progress_layer_basalt_0.png

**/

#include <pebble.h>
#include <progress_layer.h>

int main(void) {
  Window* my_window = window_create();
  window_stack_push(my_window, true);
  Layer* root_layer = window_get_root_layer(my_window);

  GRect root_frame = layer_get_frame(root_layer);

  GRect frame;
  frame.size = GSize(80, 30);
  frame.origin = GPoint((root_frame.size.w - frame.size.w)/2, (root_frame.size.h - frame.size.h)/2);

  ProgressLayer* progress_layer = progress_layer_create(frame, 0.0, 1.0);
  progress_layer_set_value(progress_layer, 0.33, "33%");

  layer_add_child(root_layer, progress_layer_get_layer(progress_layer));
  app_event_loop();
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
