/**
\example example_progressbar_layer.c

An example on how to use progressbar_layer.h

\image html example_progressbar_layer_basalt_0.png

**/

#include <pebble.h>
#include <progressbar_layer.h>

int main(void) {
  Window* my_window = window_create();
  window_stack_push(my_window, true);
  Layer* root_layer = window_get_root_layer(my_window);

  GRect root_frame = layer_get_frame(root_layer);

  GRect frame;
  frame.size = GSize(80, 30);
  frame.origin = GPoint((root_frame.size.w - frame.size.w)/2, (root_frame.size.h - frame.size.h)/2);

  ProgressBarLayer* progressbar_layer = progressbar_layer_create(frame, 0.0, 1.0);
  progressbar_layer_set_value(progressbar_layer, 0.33);

  layer_add_child(root_layer, progressbar_layer_get_layer(progressbar_layer));
  app_event_loop();
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
