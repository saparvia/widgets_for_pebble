/**
\example example_stack_layer.c

An example on how to use stack_layer.h

\image html example_stack_layer_basalt_0.png

**/

#include <pebble.h>
#include "stack_layer.h"

int main(void) {
  Window* my_window = window_create();
  window_stack_push(my_window, true);

  Layer* root_layer = window_get_root_layer(my_window);

  GRect root_frame = layer_get_frame(root_layer);

  StackLayer* stack_layer = stack_layer_create(root_frame, STACK_VERTICAL);

  TextLayer* layer1 = text_layer_create(GRect(0,0,0,20));
  text_layer_set_text(layer1, "Layer 1");
  text_layer_set_text_alignment(layer1, GTextAlignmentCenter);
  stack_layer_add_layer(stack_layer, text_layer_get_layer(layer1));

  TextLayer* layer2 = text_layer_create(GRect(0,0,0,20));
  text_layer_set_text(layer2, "Layer 2");
  text_layer_set_text_alignment(layer2, GTextAlignmentCenter);
  stack_layer_add_layer(stack_layer, text_layer_get_layer(layer2));

  layer_add_child(root_layer, stack_layer_get_layer(stack_layer));
  app_event_loop();
}

/* vim: set expandtab tabstop=2 shiftwidth=2: */
