*Widgets for Pebble* is a collection of helpful layers and other functions to aid in the development of Pebble apps.

Current the following are included
* StackLayer - A layer which stacks child Layers either beneath or next to each other (stack_layer.h)
* ProgressBarLayer - A layer which shows a progress bar (progressbar_layer.h)
* ProgressLayer - A layer which shows a progress bar and a textual representation of the progress (progress_layer.h)
* ProgressWindow - A window which shows a title and a ProgressLayer (progress_window.h)

See the header files for documentation.

\image html example_demo_basalt_0.png

\image html example_demo_basalt_1.png
